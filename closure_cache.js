
// Function cacheFunc(cb)

function cacheFunc(cb, arg){
    cache = {};
    argument = arg;
    if(typeof cb == 'function'){
        function cacheCheck(argument){
            if(this.argument in cache){
                console.log('cache implemented');
                return cache[this.argument]
            }
            else{
                cache[this.argument] = cb(this.argument);
                console.log('cache not implemented');
                return cache[this.argument];
            }
        }
        return cacheCheck;
    }
    else{
        return cache;
    }
}

module.exports = cacheFunc;

//End of Cache function
//---------------------------------------------------------