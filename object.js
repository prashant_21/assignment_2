//Function keys_new(obj)

let k = [];
function keys_new(obj){
    if(obj instanceof Object){
        for(let i in obj){
            if(obj.hasOwnProperty(i)){
                k.push(i);
            }
        }
        return k;
    }
    else{
        return k;
    }
}

module.exports = keys_new;

//End of Key function
//-----------------------------------------------------------------------

//Function values(obj)

let v = [];
function values_new(obj){
    if(obj instanceof Object){
        for(let i in obj){
            if(obj.hasOwnProperty(i)){
                v.push(obj[i]);
            }
        }
        return v;
    }
    else{
        return v;
    }
}

module.exports = values_new;

//End of Value function
//------------------------------------------------------------------------

//Function mapObject(obj, cb)

function map_new(obj,cb){
    if(obj instanceof Object && typeof cb === 'function'){
        for(let i in obj){
            if(obj.hasOwnProperty(i)){
               obj[i] = cb(obj[i],i);
            }
        }
        return obj;
    }
    else{
        return obj;
    }
}

module.exports = map_new;

//End of mapObject function
//-------------------------------------------------------------------------

//Function pairs(obj)

let p = [];
function pairs_new(obj){
    if(obj instanceof Object){
        for(let i in obj){
            if(obj.hasOwnProperty(i)){
                p.push([i,obj[i]]);
            }
        }
        return p;
    }
    else{
        return p;
    }
}

module.exports = pairs_new;

//End of Pairs function
//-------------------------------------------------------------------------

//Function invert(obj)

let invert = {};
function invert_new(obj){
    if(obj instanceof Object){
        for(let i in obj){
            invert[obj[i]] = i; 
        }
        return invert;
    }
    else{
        return invert;
    }
}

module.exports = invert_new;

//End of Invert function
//--------------------------------------------------------------------------

//Function defaults(obj, defaultProps)

function defaults_new(obj, defaultProps){
    let props = defaultProps;
    if(obj instanceof Object && defaultProps instanceof Object){
        for(let i in props){
            if(obj[i] === undefined){
                obj[i] = props[i];
            }
        }
        return obj;
    }
    else{
        return obj;
    }
}

module.exports = defaults_new;