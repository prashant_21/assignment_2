//Each Function

function each(item, cb){
    if(Array.isArray(item) && typeof cb === 'function'){
        for(let i = 0; i < item.length; i++){
            cb(item[i], i, item);
        }
    }
    else{
        return 0;
    }
}

module.exports = each;

//End of Each Function
//---------------------------------------------------------------------

//Map Function

let n =[];
function map_new(item, cb){
    if(Array.isArray(item) && typeof cb === 'function'){
        for(let i = 0; i < item.length; i++){
            n.push(cb(item[i], i, item));
        }
        return n;
    }
    else{
        return n;
    }
}

module.exports = map_new;

//End of Map Function
//---------------------------------------------------------------------

//Reduce Function

function reduce_new(item, cb, initial_value){
    if(Array.isArray(item) && typeof cb === 'function'){
        let accumulator = initial_value === undefined ? 0:initial_value;
        for(let i = 0; i < item.length; i++){
            accumulator = cb(accumulator, item[i]);
            console.log(accumulator,i);
        }
        return accumulator;
    }
    else{
        return 0;
    }
}

module.exports = reduce_new;

//End of Reduce Function
//----------------------------------------------------------------------

//Find Function

function find_new(item, cb){
    if(Array.isArray(item) && typeof cb === 'function'){
        for(let i = 0; i < item.length; i++){
           if(cb(item[i])){
               return item[i];
           }
        }
    }
}

module.exports = find_new;

//End of Find function
//----------------------------------------------------------------------

//Filter function

let a = [];
function filter_new(item,cb){
    if(Array.isArray(item) && typeof cb === 'function'){
        for(let i = 0; i < item.length; i++){
            if(cb(item[i])){
                a.push(item[i]);
            }
        }
        return a;
    }
    else{
        return a;
    }
}

module.exports = filter_new;

//End of Filter function
//-----------------------------------------------------------------------

//Flatten Function

let f = [];
function flat_new(item){
    if(Array.isArray(item)){
        for(let i = 0; i < item.length; i++){
            if(!Array.isArray(item[i])){
                f.push(item[i]);
            }
            if(Array.isArray(item[i])){
                for(let j = 0; j < item.length; j++){
                    flat_new(item[j]);
                }
            }
        }
        return f;
    }
    else{
        return f;
    }
}

module.exports = flat_new;

//End of Flat function
//------------------------------------------------------------------------